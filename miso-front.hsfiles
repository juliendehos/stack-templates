{-# START_FILE .gitlab-ci.yml #-}
pages:
    image: nixos/nix
    script:
        - nix-env -iA nixpkgs.cachix
        - cachix use juliendehos
        - nix-build nix/release.nix
        - mkdir -p public
        - cp result/public/* public/
    artifacts:
        paths:
            - public
    only:
        - master


{-# START_FILE app.cabal #-}
name:               app
version:            1.0
build-type:         Simple
cabal-version:      >=1.10
license:            MIT

library
  ghc-options:      -O2 -Wall
  default-language: Haskell2010
  hs-source-dirs:   src
  exposed-modules:  Lib
  build-depends:    base

executable client
  if !impl(ghcjs)
    buildable: False
  ghcjs-options:    -dedupe -O2 -Wall
  cpp-options:      -DGHCJS_BROWSER
  default-language: Haskell2010
  main-is:          Main.hs
  hs-source-dirs:   app
  build-depends:    app, base, miso

test-suite spec
  if impl(ghcjs)
    buildable: False
  main-is:          Spec.hs
  hs-source-dirs:   test
  type:             exitcode-stdio-1.0
  ghc-options:      -Wall
  default-language: Haskell2010
  other-modules:    LibSpec
  build-depends:    app, base, hspec


{-# START_FILE default.nix #-}
let pkgs = import ./nix/nixpkgs.nix ;
in {
  ghc = pkgs.haskell.packages.ghc.callCabal2nix "app" ./. {};
  ghcjs = pkgs.haskell.packages.ghcjs.callCabal2nix "app" ./. {};
}


{-# START_FILE Makefile #-}
.PHONY: test

test:
	nix-shell -A ghc.env --run "cabal --builddir=dist-ghc --config-file=config/ghc.config test"

client:
	nix-shell -A ghcjs.env --run "cabal --builddir=dist-ghcjs --config-file=config/ghcjs.config build client"
	ln -sf ../`find dist-ghcjs/ -name all.js` public/
	echo "open ./public/index.html in a browser"

release:
	nix-build nix/release.nix

clean:
	rm -rf dist-* result* public/all.js


{-# START_FILE stack.yaml #-}
resolver: lts-14.27


{-# START_FILE app/Main.hs #-}
{-# LANGUAGE OverloadedStrings #-}

import Lib

import Miso
import Miso.String (fromMisoString, MisoString, ms)

data Model = Model
    { _value :: Int
    } deriving (Eq)

data Action
    = ActionNone
    | ActionInput MisoString

main :: IO ()
main = startApp App
    { initialAction = ActionNone
    , update        = updateModel
    , view          = viewModel
    , model         = Model 0
    , subs          = []
    , events        = defaultEvents
    , mountPoint    = Nothing
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel ActionNone m = noEff m
updateModel (ActionInput v) m = noEff m { _value = read (fromMisoString v) }

viewModel :: Model -> View Action
viewModel (Model v) = div_ []
    [ h1_ [] [ text "{{name}}" ]
    , p_ [] [ input_ [onInput ActionInput, min_ "0", max_ "21", type_ "number", value_ "0"] ]
    , p_ [] [ text (ms $ "mul2(" ++ show v ++ ") = " ++ show (mul2 v))]
    ]


{-# START_FILE config/ghc.config #-}
compiler: ghc


{-# START_FILE config/ghcjs.config #-}
compiler: ghcjs


{-# START_FILE nix/nixpkgs.nix #-}
let

  bootstrap = import <nixpkgs> {};

  nixpkgs-src = bootstrap.fetchFromGitHub {
    owner = "NixOS";
    repo  = "nixpkgs";
    rev = "1c92cdaf7414261b4a0e0753ca9383137e6fef06";
    sha256 = "0d3fqa1aqralj290n007j477av54knc48y1bf1wrzzjic99cykwh";
  };

  ghc-version = "ghc865";
  ghcjs-version = "ghcjs86";

  miso-src = bootstrap.fetchFromGitHub {
    owner = "dmjio";
    repo  = "miso";
    rev = "1.4";
    sha256 = "1wl9vpqxshzrlanm9rpvgkiay3xa1abvkyknnp5z41kgfw63ypdl";
  };

  jsaddle-warp-src = bootstrap.fetchFromGitHub {
    owner = "ghcjs";
    repo  = "jsaddle";
    rev = "jsaddle-warp-0.9.5.0";
    sha256 = "068xdkyq3xdl212p9b6fkf7sajdg7n0s42mmqccvvg0n024nc3a8";
  };

  config = { 

    packageOverrides = pkgs: rec {
      haskell = pkgs.haskell // {
        packages = pkgs.haskell.packages // {

          ghc = pkgs.haskell.packages.${ghc-version}.override {
            overrides = self: super: with pkgs.haskell.lib; {

              miso = self.callCabal2nix "miso" miso-src {};

            };
          };

          } // {

          ghcjs = pkgs.haskell.packages.${ghcjs-version}.override {
            overrides = self: super: with pkgs.haskell.lib; {

              aeson = dontCheck (doJailbreak super.aeson);
              comonad = dontCheck (doJailbreak super.comonad);
              cookie = dontCheck (doJailbreak super.cookie);
              http-types = dontCheck (doJailbreak super.http-types);
              memory = dontCheck (doJailbreak super.memory);
              network = dontCheck (doJailbreak super.network_2_6_3_1);
              QuickCheck = dontCheck (doJailbreak super.QuickCheck);
              scientific = dontCheck (doJailbreak super.scientific);
              semigroupoids = dontCheck (doJailbreak super.semigroupoids);
              servant = dontCheck (doJailbreak super.servant);
              servant-client = dontCheck (doJailbreak super.servant-client);
              servant-lucid = dontCheck (doJailbreak super.servant-lucid);
              servant-websockets = dontCheck (doJailbreak super.servant-websockets);
              streaming-commons = dontCheck (doJailbreak super.streaming-commons);
              tasty = dontCheck (doJailbreak super.tasty);
              tasty-golden = dontCheck (doJailbreak super.tasty-golden);
              tasty-quickcheck = dontCheck (doJailbreak super.tasty-quickcheck);
              time-compat = dontCheck (doJailbreak super.time-compat);
              uuid-types = dontCheck (doJailbreak super.uuid-types);

              miso = self.callCabal2nix "miso" miso-src {};
              jsaddle-warp = self.callCabal2nix "jsaddle-warp" "${jsaddle-warp-src}/jsaddle-warp" {};

            };
          };

        };
      };
    };
  };

in import nixpkgs-src { inherit config; }


{-# START_FILE nix/release.nix #-}
let

  pkgs = import ./nixpkgs.nix ;
  app-src = ../. ;

  app-ghc = pkgs.haskell.packages.ghc.callCabal2nix "app" ../. {};
  app-ghcjs = pkgs.haskell.packages.ghcjs.callCabal2nix "app" ../. {};

in

  pkgs.runCommand "app" { inherit app-ghc app-ghcjs; } ''
    mkdir -p $out/public
    cp ${app-src}/public/* $out/public/
    ${pkgs.closurecompiler}/bin/closure-compiler ${app-ghcjs}/bin/client.jsexe/all.js > $out/public/all.js
	find $out/public \( -name '*.html' -o -name '*.css' -o -name '*.js' -o -name '*.svg' \) -print0 | xargs -0 ${pkgs.gzip}/bin/gzip -9 -k -f
  ''


{-# START_FILE public/index.html #-}
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> {{name}} </title>
    <script application/javascript" src="all.js" async defer> </script>
  </head>
  <body>
  </body>
</html>


{-# START_FILE src/Lib.hs #-}
-- |
-- Module: Lib
-- This is my Lib module
module Lib where

-- | Multiplies a number by two.
mul2 :: Num a => a -> a
mul2 = (*2)


{-# START_FILE test/LibSpec.hs #-}
module LibSpec (main, spec) where

import Test.Hspec

import Lib

main :: IO ()
main = hspec spec

spec :: Spec
spec = 
    describe "mul2" $ do
        it "mul2 0" $ mul2 0 `shouldBe` (0::Int)
        it "mul2 21" $ mul2 21 `shouldBe` (42::Int)
        it "mul2 (-21)" $ mul2 (-21) `shouldBe` (-42::Int)


{-# START_FILE test/Spec.hs #-}
{-# OPTIONS_GHC -F -pgmF hspec-discover #-}


