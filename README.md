# stack-templates

Some templates for creating new Haskell projects. 
See [stack documentation](https://docs.haskellstack.org/en/stable/GUIDE/#templates).

```
stack new myproj gitlab:juliendehos/simple
...
```

